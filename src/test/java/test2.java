import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObject.Page0;
import pageObject.calcScreen;
import pageObject.reg_1_step;
import pageObject.universal;

import java.net.URL;

import static data.Data0.*;
import static pageObject.Page0.*;
import static pageObject.calcScreen.*;
import static pageObject.reg_1_step.*;
import static pageObject.universal.*;

public class test2 {

    public Page0 newPage0;
    public universal newuniversal;
    public calcScreen newcalcScreen;
    public reg_1_step newreg_1_step;

    public AndroidDriver driver;
    public AppiumDriver AndroidDriver;
    private WebDriverWait WebDriverWait;

    @Test
    public void firstPageOpen() throws Exception {
        URL serverURL = new URL("http://127.0.0.1:4723/wd/hub");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("VERSION", "10");
        capabilities.setCapability("deviceName","Honor 10 Lite");
        capabilities.setCapability("platformName","Android");
        capabilities.setCapability("app", "C:/Пользователи/Valentina/Рабочий стол/app-dev-debug.apk");

        System.out.println("Step 1. Create new driver");
        AppiumDriver driver = new AndroidDriver(serverURL, capabilities);
        WebDriverWait wait = new WebDriverWait(driver, 30);
        newPage0=new Page0(driver);
        newuniversal=new universal(driver);
        newcalcScreen=new calcScreen(driver);
        newreg_1_step=new reg_1_step(driver);

        System.out.println("Step 2. Check slides");
        Thread.sleep(7000);
        //checkElementDisplayed(privacyPolicy);
        //clickOn(privacyPolicy);
        checkElementDisplayed(first);
        setSwipeNext();
        checkElementDisplayed(second);
        setSwipeNext();
        checkElementDisplayed(third);
        setSwipeNext();
        checkElementDisplayed(fourth);
        checkElementDisplayed(startButton);
        clickOn(startButton);
          //  }

    //@Test
    //public void calc_screen() throws Exception {
        System.out.println("Step 3. Calculation Screen");
        Thread.sleep(5000);

        checkElementDisplayed(title);
        checkElementDisplayed(sumCurrent);
        checkElementDisplayed(periodCurrent);
        checkElementDisplayed(selectPayType);

        clickOn(periodSeekBar);
        clickOn(sumSeekBar);
        clickOn(perMonth);
        clickOn(per2weeks);

        clickOn(receiveMoney);
    //}

    //@Test
    //public void reg_1_step() throws Exception {
        System.out.println("Step 4. registration 1 step");
        Thread.sleep(5000);

        //clickOn(agreement);
        //System.out.println("ok");
        setSwipeUp();
        //System.out.println("ok");
        //clickOn(agreeAllCheckBox);
        //System.out.println("ok");
        clickOn(agreeCheckBox);
        //clickOn(agreeCheckBox);

        //enterField(lastName, lastNameRight);
        //enterField(firstName, firstNameRight);
        //enterField(middleName, middleNameRight);
        enterField(email, emailRight);

        enterField(phone, phoneRight);

        clickOn(ContinueButton);
        //clickOn(codeInput);
        Thread.sleep(5000);
        clickOn(codeInput);
        enterField(codeInput, passwordRight);



    }


    @Test
    public void CloseApp() {

        System.out.println("Close driver");
        if (driver!=null) {
            driver.quit();
        }
    }
}
