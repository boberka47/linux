package pageObject;

import io.appium.java_client.TouchAction;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;


public class universal {

    public static WebDriver driver;
    public Object MobileDriver;

    public universal(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public static void setSwipeNext () {
        new TouchAction((io.appium.java_client.MobileDriver) driver).press(916,951).moveTo(210,943).release().perform();
    }

    public static void setSwipeUp () {
        new TouchAction((io.appium.java_client.MobileDriver) driver).press(900,900).moveTo(900,500).release().perform();
    }

    public static void checkElementDisplayed(WebElement element) {
        Assert.assertTrue(element.isDisplayed());
    }

    public static void clickOn(WebElement element){
        element.click();
    }

    public static void enterField(WebElement element, String value) {
        element.sendKeys(value);
    }


}
