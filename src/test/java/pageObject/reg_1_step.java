package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class reg_1_step {
    public static WebDriver driver;
    private Object MobileDriver;

    @FindBy(id = "ru.robocredit.installment:id/lastName")
    public static WebElement lastName;

    @FindBy(id = "ru.robocredit.installment:id/firstName")
    public static WebElement firstName;

    @FindBy(id = "ru.robocredit.installment:id/middleName")
    public static WebElement middleName;

    @FindBy(id = "ru.robocredit.installment:id/email")
    public static WebElement email;

    @FindBy(id = "ru.robocredit.installment:id/phone")
    public static WebElement phone;

    @FindBy(id = "ru.robocredit.installment:id/agreementCheckboxText")
    public static WebElement agreement;

    @FindBy(id = "ru.robocredit.installment:id/agreeAllCheckbox")
    public static WebElement agreeAllCheckBox;

    @FindBy(id = "ru.robocredit.installment:id/continueButton")
    public static WebElement ContinueButton;

    @FindBy(id = "ru.robocredit.installment:id/agreementCheckbox")
    public static WebElement agreeCheckBox;

    //@FindBy(id = "ru.robocredit.installment:id/codeInputView")
    //public static WebElement codeInput;

    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.View")
    public static WebElement codeInput;

    public reg_1_step(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
