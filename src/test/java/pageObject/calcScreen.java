package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class calcScreen {

    public static WebDriver driver;
    public Object MobileDriver;

    @FindBy(id = "ru.robocredit.installment:id/title")
    public static WebElement title;

    @FindBy(id = "ru.robocredit.installment:id/sumCurrent")
    public static WebElement sumCurrent;

    @FindBy(id = "ru.robocredit.installment:id/periodCurrent")
    public static WebElement periodCurrent;

    @FindBy(id = "ru.robocredit.installment:id/tvSelectPayType")
    public static WebElement selectPayType;

    @FindBy(id = "ru.robocredit.installment:id/sumSeekBar")
    public static WebElement sumSeekBar;

    @FindBy(id = "ru.robocredit.installment:id/periodSeekBar")
    public static WebElement periodSeekBar;

    @FindBy(id = "ru.robocredit.installment:id/perMonth")
    public static WebElement perMonth;

    @FindBy(id = "ru.robocredit.installment:id/per2weeks")
    public static WebElement per2weeks;

    @FindBy(id = "ru.robocredit.installment:id/btnReceiveMoney")
    public static WebElement receiveMoney;

    public calcScreen(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
