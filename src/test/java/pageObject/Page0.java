package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Page0{

    public static WebDriver driver;
    private Object MobileDriver;

    @FindBy(id = "ru.robocredit.installment:id/privacyPolicyText")
    public static WebElement privacyPolicy;

    @FindBy(xpath = ".//*[@class='android.widget.TextView' and @text='Визиты в отделения и долгий сбор документов - в прошлом! Заполните анкету и получите займ до 100 000 рублей не выходя из дома.']")
    public static WebElement first;

    @FindBy(xpath = ".//*[@class='android.widget.TextView' and @text='Решение по займу принимается автоматически в течение 2 минут. Обработка данных происходит круглосуточно.']")
    public static WebElement second;

    @FindBy(xpath = ".//*[@class='android.widget.TextView' and @text='Мы выдаем крупные суммы, при этом наши процентные ставки максимально приближены к банковским кредитам - одни из самых низких на рынке!']")
    public static WebElement third;

    @FindBy(xpath = ".//*[@class='android.widget.TextView' and @text='Вы сами выбираете срок и сумму займа и определяете размер итоговых выплат. Можно вносить платеж 1 или 2 раза в месяц по вашему выбору.']")
    public static WebElement fourth;

    @FindBy(id = "ru.robocredit.installment:id/startButton")
    public static WebElement startButton;

    @FindBy(id = "ru.robocredit.installment:id/title")
    public static WebElement title_1;

    public Page0(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
